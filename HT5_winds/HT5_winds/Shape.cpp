#include "Shape.h"
#include <iostream>

Shape::Shape(COORD c, int color){
	this->c = c;
	this->color = color;
}

Shape::Shape(){
	this->c = { 0, 0 };
	this->color = 0x00;
}

void Shape::SetColor(int color){
	this->color = color;
}

void Shape::SetSize(int hsize, int vsize){
	this->hsize = hsize;
	this->vsize = vsize;
}

int Shape::GetColor(void){
	return this->color;
}

COORD Shape::GetCoord(void){
	return c;
}

string Shape::GetLabel(void){
	return text;
}

void Shape::SetCoord(COORD c){
	this->c = c;
}

void Shape::SetLabel(string str)
{
	text = str;
}

void Shape::Draw(HANDLE hOut){
	SetConsoleCursorPosition(hOut, c);//����������� ������ � ����������
	SetConsoleTextAttribute(hOut, color);
	COORD t = c;
	for (int j = 0; j < vsize; j++)	{
		for (int i = 0; i < hsize; i++){
			if (i == 0 && j == 0)
			{
				std::cout << (char)201;
			}
			else if (i == 0 && j == vsize -1)
			{
				std::cout << (char)200;
			}
			else if (i == hsize-1 && j == 0)
			{
				std::cout << (char)187;
			}
			else if (i == hsize - 1 && j == vsize-1)
			{
				std::cout << (char)188;
			}
			else if (j == 0 || j == vsize-1)
			{
				std::cout << (char)205;
			}
			else if (i == 0 || i == hsize-1)
			{
				std::cout << (char)186;
			}
			else
			{
				std::cout << ' ';
			}
		}
		t.Y++;
		SetConsoleCursorPosition(hOut, t);
	}
}

bool Shape::CheckHit(COORD c){
	if (c.X >= this->c.X && c.X < this->c.X + hsize && c.Y >= this->c.Y && c.Y < this->c.Y + vsize)
	{
		return true;
	}
	return false;
}

void Shape::Move(HANDLE hOut, COORD c){
	SetConsoleCursorPosition(hOut, this->c);//����������� ������ � ����������
	SetConsoleTextAttribute(hOut, 0x00);
	COORD t = this->c;
	for (int j = 0; j < vsize; j++)	{
		for (int i = 0; i < hsize; i++){
			std::cout << ' ';
		}
		t.Y++;
		SetConsoleCursorPosition(hOut, t);
	}

	this->c = c;

	Draw(hOut);

}

Shape::~Shape(){

}
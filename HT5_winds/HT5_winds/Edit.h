#pragma once
#include "Shape.h"
class Edit :
	public Shape
{
	bool checked = false;
	string attr;
public:
	Edit();
	Edit(COORD c, int color);
	void SetCheckResult(string attr);

	virtual void Draw(HANDLE hOut);
	~Edit();
};


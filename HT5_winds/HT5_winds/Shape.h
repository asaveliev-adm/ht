#pragma once
#include "Headers.h"
#include <Windows.h>


class Shape
{
protected:
	COORD c;
	int color;
	int vsize = 3;
	int hsize = 8;
	string text;
public:
	Shape();
	Shape(COORD c, int color);
	void SetColor(int color);
	void SetCoord(COORD c);
	void SetSize(int hsize, int vsize);
	virtual void SetLabel(string str);
	int GetColor(void);
	virtual string GetLabel(void);
	COORD GetCoord(void);
	virtual void Draw(HANDLE hOut);

	bool CheckHit(COORD c);
	virtual void Move(HANDLE hOut, COORD c);
	~Shape();
};



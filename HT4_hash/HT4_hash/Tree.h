#include <iostream>
using namespace std;

template <class K, class T>
struct Elem
{
	T value;
	K key;
	Elem<K,T> * left, *right, *parent;
	Elem(){ left = right = parent = nullptr; }
	Elem(const K & k, const T & v){ value = v; key = k; left = right = parent = nullptr; }
	//Elem<K, T> & operator=(const Elem<K, T> & e){key = e->key;value = e->value;}
};

template <class K, class T>
class Tree
{
	Elem<K,T> * root;
public:
	Tree();
	~Tree(){ Del(); };
	Tree(const Tree<K,T> & t);
	void Print(Elem<K,T> * node);
	void Insert(const K & key, const T & val);
	Elem<K,T>* GetRoot(){ return root; }
	Elem<K,T>* Search(const K & key);
	Elem<K,T>* Max(Elem<K,T> * node);
	Elem<K,T>* Min(Elem<K,T> * node);
	Elem<K,T>* Prev(Elem<K,T> * node);
	Elem<K,T>* Next(Elem<K,T> * node);
	void Del(Elem<K, T> * node = nullptr);
	Elem<K,T>* Clone(Elem<K,T>* el);
	Tree<K,T> & operator=(const Tree<K, T> & t);
	T & operator[](const K & key);
};

template <class K, class T>
Tree<K,T>::Tree()
{
	root = nullptr;
}

template <class K, class T>
Tree<K,T> & Tree<K,T>::operator=(const Tree<K,T> & t)
{
	if (this == &t)
		return *this;
	Del();
	root = Clone(t.root);
	return *this;
}

template <class K, class T>
Tree<K,T>::Tree(const Tree<K,T> & t)
{
	root = Clone(t.root);
}

template <class K, class T>
Elem<K,T>* Tree<K,T>::Clone(Elem<K,T>* el)
{
	if (el)
	{
		Elem<K,T> * temp = new Elem<K,T>(el->key, el->value);
		if (el->left)
		{
			temp->left = Clone(el->left);
			temp->left->parent = temp;
		}
		if (el->right)
		{
			temp->right = Clone(el->right);
			temp->right->parent = temp;
		}
		return temp;
	}
	else
		return el;
}

template <class K, class T>
void Tree<K,T>::Del(Elem<K,T> * node = nullptr)
{
	Elem<K,T> * temp;
	if (node)
	{
		if (!node->left && !node->right)
		{
			if (node->parent)
			{
				if (node->parent->left == node)
					node->parent->left = nullptr;
				else
					node->parent->right = nullptr;
			}
			else
			{
				root = nullptr;
			}
			delete node;
			return;
		}
		else if (node->left && node->right)
		{
			temp = Next(node);
			node->key = temp->key;
			node->value = temp->value;
			node = temp;
		}
		if (node->left)
			temp = node->left;
		else
			temp = node->right;
		if (temp)
			temp->parent = node->parent;
		if (node->parent)
		{
			if (node->parent->left == node)
				node->parent->left = temp;
			else
				node->parent->right = temp;
		}
		else
			root = temp;
		delete node;
	}
	else
	{
		while (root)
			Del(root);
	}
}

template <class K, class T>
void Tree<K,T>::Print(Elem<K,T> * node)
{
	if (!node)
		return;
	Print(node->left);
	cout << node->key << ": "<< node->value << endl;
	Print(node->right);
}

template <class K, class T>
Elem<K,T>* Tree<K,T>::Max(Elem<K,T> * node)
{
	Elem<K,T> * temp = node;
	if (!temp)
		return temp;
	while (temp->right)
		temp = temp->right;
	return temp;
}

template <class K, class T>
Elem<K,T>* Tree<K,T>::Min(Elem<K,T> * node)
{
	Elem<K,T> * temp = node;
	if (!temp)
		return temp;
	while (temp->left)
		temp = temp->left;
	return temp;
}

template <class K, class T>
Elem<K,T>* Tree<K,T>::Prev(Elem<K,T> * node)
{
	if (!node)
		return node;
	if (node->left)
		return Max(node->left);
	Elem<K,T> * temp = node;
	while (temp->parent && temp->parent->left == temp)
	{
		temp = temp->parent;
	}
	return temp->parent;
}

template <class K, class T>
Elem<K,T>* Tree<K,T>::Next(Elem<K,T> * node)
{
	if (!node)
		return node;
	if (node->right)
		return Min(node->right);
	Elem<K,T> * temp = node;
	while (temp->parent && temp->parent->right == temp)
	{
		temp = temp->parent;
	}
	return temp->parent;
}

template <class K, class T>
Elem<K,T>* Tree<K,T>::Search(const K & key)
{
	Elem<K,T> * temp = root;
	while (temp && temp->key != key)
	{
		if (key < temp->key)
			temp = temp->left;
		else
			temp = temp->right;
	}
	return temp;
}

template <class K, class T>
void Tree<K,T>::Insert(const K & key, const T & val)
{
	Elem<K,T> * newnode = new Elem<K,T>(key,val);
	if (!root)
	{
		root = newnode;
		return;
	}
	Elem<K,T> * temp = root, *temp2;
	do{
		temp2 = temp;
		if (key < temp->key)
			temp = temp->left;
		else
			temp = temp->right;
	} while (temp);
	newnode->parent = temp2;
	if (key < temp2->key)
		temp2->left = newnode;
	else
		temp2->right = newnode;
}

template <class K, class T>
T & Tree<K,T>::operator[](const K & key)
{
	//Elem<K, T>* Search(const K & key);
	Elem<K, T> * temp = Search(key);
	if (!temp)
	{
		T val = (T)0;
		this->Insert(key, val);
		temp = Search(key);
	}
	return temp->value;
}

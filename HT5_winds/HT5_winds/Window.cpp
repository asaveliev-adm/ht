#include "Window.h"



Window::Window(COORD c, int color) : Shape(c, color)
{
	short HeadesSize = 3;
	vsize = 15;
	hsize = 40;
	WElements.push_back(new Button( c, 0x50));
	WElements.back()->SetLabel("Windows");
	WElements.back()->SetSize(hsize, HeadesSize);
	WElements.push_back(new Button({ c.X + hsize - 9, c.Y + vsize - 4 }, 0x78));
	WElements.back()->SetLabel("Exit");
	WElements.push_back(new Button({ c.X + 1, c.Y + vsize - 4 }, 0x78));
	WElements.back()->SetLabel("Check");
	WElements.push_back(new Edit({ c.X+2 , c.Y+4 }, 0x70));
	WElements.back()->SetSize(hsize-4, vsize - HeadesSize-6);
}

void Window::SpelDraw(HANDLE hOut, const set<string> & dict){
	vector<string> arr;
	string str = WElements.back()->GetLabel();
	string delim(" ");
	size_t prev = 0;
	size_t next;
	size_t delta = delim.length();
	while ((next = str.find(delim, prev)) != string::npos){
		arr.push_back(str.substr(prev, next - prev));
		prev = next + delta;
	}
	arr.push_back(str.substr(prev));	

	string tmp;
	for (unsigned int i = 0; i < arr.size(); i++)
	{
		bool find = (dict.find(arr[i]) != dict.end());
		int ttmp = arr[i].size();
		for (int j = 0; j < ttmp; j++)
		{
			if (find)
			{
				tmp += 0x70;
			}
			else
			{
				tmp += 0x7C;
			}			
		}
		tmp += 0x70;
	}
	((Edit*)WElements.back())->SetCheckResult(tmp);
	Draw(hOut);
}

void Window::Draw(HANDLE hOut)
{
	Shape::Draw(hOut);
	for (auto it = WElements.begin(); it != WElements.end(); it++)
	{
		(*it)->Draw(hOut);
	}
}

int Window::CheckBtnHit(COORD c){
	for (unsigned int i = 1; i <= WElements.size(); i++)
	{
		if (WElements[i-1]->CheckHit(c))
		{
			return i;
		}		
	}
	return 0;
}

void Window::Move(HANDLE hOut, COORD c){
	int dx = c.X - this->c.X;
	int dy = c.Y - this->c.Y;
	COORD tmpc;
	this->c = c;
	for (auto it = WElements.begin(); it != WElements.end(); it++)
	{
		tmpc = (*it)->GetCoord();
		tmpc.X += dx;
		tmpc.Y += dy;
		(*it)->SetCoord(tmpc);
	}
}

void Window::SetLabel(string str){
	WElements.back()->SetLabel(str);
}

string Window::GetLabel(void){
	return WElements.back()->GetLabel();
}
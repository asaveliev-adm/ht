#pragma once
#include "Shape.h"
#include "Headers.h"


class Button : public Shape
{
public:
	Button();
	Button(COORD c, int color);
	virtual void Draw(HANDLE hOut);
	~Button();
};


#pragma once
#include "Headers.h"
#include "Edit.h"
#include "Button.h"
#include "Shape.h"
#include "Os.h"
#include <sstream>


class Window : public Shape
{
	vector<Shape*> WElements;
public:
	Window();
	Window(COORD c, int color);
	virtual void Draw(HANDLE hOut);
	void SpelDraw(HANDLE hOut, const set<string> & dict);
	virtual void Move(HANDLE hOut, COORD c);
	virtual void SetLabel(string str);
	virtual string GetLabel(void);
	int CheckBtnHit(COORD c);




	
};
